/* Dooba SDK
 * STA013 MP3 Decoder Driver
 */

// External Includes
#include <util/delay.h>
#include <spi/spi.h>
#include <i2c/i2c.h>
#include <dio/dio.h>

// Internal Includes
#include "config.h"
#include "sta013.h"

// Pins
uint8_t sta013_pin_rst;
uint8_t sta013_pin_dreq;
uint8_t sta013_pin_cs;

// Initialize
uint8_t sta013_init(uint8_t rst_pin, uint8_t dreq_pin, uint8_t cs_pin)
{
	// Pre-Init
	sta013_preinit(rst_pin, dreq_pin, cs_pin);

	// Set Pins
	sta013_pin_rst = rst_pin;
	sta013_pin_dreq = dreq_pin;
	sta013_pin_cs = cs_pin;

	// Delay
	_delay_ms(STA013_DELAY_INIT);

	// Start Chip
	dio_hi(sta013_pin_rst);

	// Delay
	_delay_ms(STA013_DELAY_INIT);

	// Identify Chip
	if(i2c_read_reg(STA013_I2C_ADDR, STA013_REG_IDENT) != STA013_CHIP_IDENT)				{ return 1; }

	// Reset
	i2c_write_reg(STA013_I2C_ADDR, STA013_REG_SOFT_RESET, 1);
	_delay_ms(STA013_DELAY_RESET);
	i2c_write_reg(STA013_I2C_ADDR, STA013_REG_SOFT_RESET, 0);

	// Re-Identify Chip
	if(i2c_read_reg(STA013_I2C_ADDR, STA013_REG_IDENT) != STA013_CHIP_IDENT)				{ return 1; }

	// Send Configuration Data
	sta013_send_config();

	// Start Running
	i2c_write_reg(STA013_I2C_ADDR, STA013_REG_RUN, STA013_RUN_FLAG_RUN);

	// Pause
	sta013_pause();

	return 0;
}

// Pre-Initialize
void sta013_preinit(uint8_t rst_pin, uint8_t dreq_pin, uint8_t cs_pin)
{
	// Configure Pins
	dio_output(rst_pin);
	dio_input(dreq_pin);
	dio_output(cs_pin);

	// Initialize Pins
	dio_lo(rst_pin);
	dio_lo(cs_pin);
}

// Send Configuration Data
void sta013_send_config()
{
	uint16_t i;
	uint8_t addr;
	uint8_t val;

	// Loop through Configuration Entries
	for(i = 0; i < STA013_CONFIG_SIZE; i = i + 1)
	{
		// Pull up Configuration Data
		addr = pgm_read_byte(&(sta013_config_data[i * 2]));
		val = pgm_read_byte(&(sta013_config_data[(i * 2) + 1]));

		// Write Configuration Value
		i2c_write_reg(STA013_I2C_ADDR, addr, val);

		// Delay on Reset
		if(addr == STA013_REG_SOFT_RESET)													{ _delay_ms(STA013_DELAY_RESET); }

		// Delay
		_delay_us(STA013_DELAY_CONFIG);
	}
}

// Play
void sta013_play()
{
	// Set Playing
	i2c_write_reg(STA013_I2C_ADDR, STA013_REG_PLAY, STA013_PLAY_FLAG_PLAY);
}

// Pause
void sta013_pause()
{
	// Set Paused
	i2c_write_reg(STA013_I2C_ADDR, STA013_REG_PLAY, 0);
}

// Set Channel Mix
void sta013_set_channel_mix(uint8_t r_into_l, uint8_t l_into_r)
{
	// Set Channel Mix Registers
	i2c_write_reg(STA013_I2C_ADDR, STA013_REG_DLB, l_into_r);
	i2c_write_reg(STA013_I2C_ADDR, STA013_REG_DRB, r_into_l);
}

// Set Attenuation
void sta013_set_attenuation(uint8_t r, uint8_t l)
{
	// Set Attenuation Registers
	i2c_write_reg(STA013_I2C_ADDR, STA013_REG_DLA, l);
	i2c_write_reg(STA013_I2C_ADDR, STA013_REG_DRA, r);
}

// Set Bass Adjust
void sta013_set_bass(uint16_t freq, uint8_t v)
{
	// Set Bass Registers
	i2c_write_reg(STA013_I2C_ADDR, STA013_REG_BFL, freq & 0x00ff);
	i2c_write_reg(STA013_I2C_ADDR, STA013_REG_BFH, (freq >> 8) & 0x00ff);
	i2c_write_reg(STA013_I2C_ADDR, STA013_REG_BAE, v);
}

// Set Treble Adjust
void sta013_set_treble(uint16_t freq, uint8_t v)
{
	// Set Treble Registers
	i2c_write_reg(STA013_I2C_ADDR, STA013_REG_TFL, freq & 0x00ff);
	i2c_write_reg(STA013_I2C_ADDR, STA013_REG_TFH, (freq >> 8) & 0x00ff);
	i2c_write_reg(STA013_I2C_ADDR, STA013_REG_TAE, v);
}

// Set Tone Attenuation
void sta013_set_tone_attenuation(uint8_t v)
{
	// Set Tone Attenuation
	i2c_write_reg(STA013_I2C_ADDR, STA013_REG_TON, v);
}

// Send MP3 Data
uint16_t sta013_send_data(uint8_t *d, uint16_t s)
{
	uint16_t i = 0;

	// Select STA013
	dio_hi(sta013_pin_cs);

	// Transfer Data
	while((i < s) && (dio_rd(sta013_pin_dreq)))												{ SPDR = d[i]; while(!(SPSR & _BV(SPIF))) { /* NoOp */ } i = i + 1; }

	// De-Select STA013
	dio_lo(sta013_pin_cs);

	return i;
}

/* Dooba SDK
 * STA013 MP3 Decoder Driver
 */

#ifndef	__STA013_H
#define	__STA013_H

// External Includes
#include <stdint.h>
#include <avr/io.h>

// Registers
#define	STA013_REG_IDENT			0x01
#define	STA013_REG_SOFT_RESET		0x10
#define	STA013_REG_PLAY				0x13
#define	STA013_REG_RUN				0x72
#define	STA013_REG_DLA				0x46
#define	STA013_REG_DLB				0x47
#define	STA013_REG_DRA				0x48
#define	STA013_REG_DRB				0x49
#define	STA013_REG_TFL				0x77
#define	STA013_REG_TFH				0x78
#define	STA013_REG_BFL				0x79
#define	STA013_REG_BFH				0x7a
#define	STA013_REG_TAE				0x7b
#define	STA013_REG_BAE				0x7c
#define	STA013_REG_TON				0x7d

// Chip Signature (returned by IDENT Register)
#define	STA013_CHIP_IDENT			0xac

// Run Flags
#define	STA013_RUN_FLAG_RUN			0x01

// Play Flags
#define	STA013_PLAY_FLAG_PLAY		0x01

// Delays
#define	STA013_DELAY_INIT			50
#define	STA013_DELAY_CONFIG			10
#define	STA013_DELAY_RESET			100

// I2C Address
#define	STA013_I2C_ADDR				0x86

// Initialize
extern uint8_t sta013_init(uint8_t rst_pin, uint8_t dreq_pin, uint8_t cs_pin);

// Pre-Initialize
extern void sta013_preinit(uint8_t rst_pin, uint8_t dreq_pin, uint8_t cs_pin);

// Send Configuration Data
extern void sta013_send_config();

// Play
extern void sta013_play();

// Pause
extern void sta013_pause();

// Set Channel Mix
extern void sta013_set_channel_mix(uint8_t r_into_l, uint8_t l_into_r);

// Set Attenuation
extern void sta013_set_attenuation(uint8_t r, uint8_t l);

// Set Bass Adjust
extern void sta013_set_bass(uint16_t freq, uint8_t v);

// Set Treble Adjust
extern void sta013_set_treble(uint16_t freq, uint8_t v);

// Set Tone Attenuation
extern void sta013_set_tone_attenuation(uint8_t v);

// Send MP3 Data
extern uint16_t sta013_send_data(uint8_t *d, uint16_t s);

#endif

/* Dooba SDK
 * STA013 MP3 Decoder Driver
 */

#ifndef	__STA013_CONFIG_H
#define	__STA013_CONFIG_H

// External Includes
#include <stdint.h>
#include <avr/io.h>
#include <avr/pgmspace.h>

// Configuration Size (Entry Count)
#define	STA013_CONFIG_SIZE 2013

// Configuration Data Size (Bytes)
#define	STA013_CONFIG_DATA_SIZE (STA013_CONFIG_SIZE * 2)

// Configuration Data
extern const uint8_t sta013_config_data[STA013_CONFIG_DATA_SIZE] PROGMEM;

#endif
